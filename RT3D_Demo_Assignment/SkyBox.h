#pragma once
#include "rt3d.h"
#include "rt3dObjLoader.h"

class SkyBox
{
public:
	SkyBox();
	~SkyBox(void);
	void setup();
	GLuint mesh(), front(), back(), left(), right(), top();
	
private:
	enum Face {FRONT, BACK, LEFT, RIGHT, TOP};
	GLuint face;
	GLuint skyboxTexture[6];
	GLuint meshIndexCount;
	GLfloat r;
	int numFaces;
	std::stack<glm::mat4> mvStack;
	glm::mat4 modelview;
			
};

