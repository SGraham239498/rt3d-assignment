#pragma once
#include "rt3d.h"
#include "Character.h"


class Camera
{
public:
	Camera(Character* playerToView);
	~Camera(void);
	void update();
	void handleEvent();
	glm::mat4 setViewMatrix();
	
private:
	glm::vec3 eye;
	glm::vec3 at;
	glm::vec3 up;
	GLfloat r;
	Character * player;
	float camangle; 
};

