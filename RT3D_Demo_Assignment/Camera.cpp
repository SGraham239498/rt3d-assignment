#include "Camera.h"


Camera::Camera(Character* playerToView)
{
	player = playerToView;
	eye = glm::vec3(0.0f, 1.0f, 4.0f);
	at = glm::vec3(0.0f, 1.0f, 3.0f);
	up = glm::vec3(0.0f, 1.0f, 0.0f);
	r = 0.0f;
	
}


Camera::~Camera(void)
{
}

void Camera::update()
{
	camangle = -player->getcR();
	eye = glm::vec3(player->xPos() + 3*std::sin(camangle*DEG_TO_RAD), player->yPos()+0.5f, player->zPos() + 3*std::cos(camangle*DEG_TO_RAD));
	at = glm::vec3(player->xPos(), player->yPos(), player->zPos());
}

void Camera::handleEvent()
{
		
}


glm::mat4 Camera::setViewMatrix()
{
	glm::mat4 view = glm::lookAt(eye,at,up);
	return view;
}