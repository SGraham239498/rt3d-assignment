#include "SkyBox.h"

SkyBox::SkyBox(void)
{
		
}


SkyBox::~SkyBox(void)
{
}

void SkyBox::setup()
{
	r = 90;
	meshIndexCount = 0;
	numFaces = 6;
	// load skybox plane
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	rt3d::loadObj("skyboxPlane.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	meshIndexCount = size;
	face = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());

	skyboxTexture[FRONT] = rt3d::loadBitmapClamp("nuke_front.bmp");
	skyboxTexture[BACK] = rt3d::loadBitmapClamp("nuke_back.bmp");
	skyboxTexture[LEFT] = rt3d::loadBitmapClamp("nuke_left.bmp");
	skyboxTexture[RIGHT] = rt3d::loadBitmapClamp("nuke_right.bmp");	
	skyboxTexture[TOP] = rt3d::loadBitmapClamp("nuke_top.bmp");
		
	
}

GLuint SkyBox::mesh(){ return face;}

GLuint SkyBox::front(){ return skyboxTexture[FRONT];}

GLuint SkyBox::back(){ return skyboxTexture[BACK];}

GLuint SkyBox::left(){ return skyboxTexture[LEFT];}

GLuint SkyBox::right(){ return skyboxTexture[RIGHT];}

GLuint SkyBox::top(){ return skyboxTexture[TOP];}


