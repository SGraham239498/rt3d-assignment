#pragma once
#include "rt3d.h"
#include "rt3dObjLoader.h"
#include "Character.h"

class BoundingRect
{
public:
	BoundingRect(void);
	~BoundingRect(void);
	void init(GLuint& shader, GLuint& meshToUse, GLuint& indexToUse, float x, float z, glm::vec3& ps);
	glm::vec3 getPos();
	void setPos(glm::vec3 newPos) {pos = newPos;}
	float xPos();
	float yPos();
	float zPos();
	float getcR(){return cR;}
	void update();
	void handleEvent(Character * player);
	void draw(GLuint& shader, glm::mat4& mainMatrix);
	
private:
	glm::vec3 pos;
	std::stack<glm::mat4> mvStack;
	float scaleX;
	float scaleZ;
	float mR; // model rotation
	float cR; // character rotation
	GLuint mesh;
	GLuint index;
	float width;
	float depth;
	
};

