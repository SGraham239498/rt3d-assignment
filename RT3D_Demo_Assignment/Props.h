#pragma once
#include "rt3d.h"
#include "rt3dObjLoader.h"

//  Ideally each prop would be a seperate object and possibly inherit from a generic class
class Props 
{
public:
	Props(void);
	virtual ~Props(void);
	void init(GLuint& shader);
	glm::vec3 getPos();
	glm::vec3 setPos(glm::vec3 newPos);
	float xPos();
	float yPos();
	float zPos();
	void loadBarrier(GLuint& shader);
	void drawBarrier(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot);
	void loadBridge(GLuint& shader);
	void drawBridge(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot);
	void loadOldCar(GLuint& shader);
	void drawOldCar(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot);
	void loadFence(GLuint& shader);
	void drawFence(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot);
	void loadRock(GLuint& shader);
	void drawRock(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot);
	void loadBrokenWall(GLuint& shader);
	void drawBrokenWall(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot);
	
private:
	glm::vec3 pos;
	std::stack<glm::mat4> mvStack;
	float scale;
	float mR; // model rotation
	GLuint barrierIndex, bridgeIndex, oldCarIndex, fenceIndex, rockIndex, brokenWallIndex;
	GLuint barrierMesh, bridgeMesh, oldCarMesh, fenceMesh, rockMesh, brokenWallMesh;
	GLuint barrierTex, bridgeTex, oldCarTex, fenceTex, rockTex, brokenWallTex;
};

