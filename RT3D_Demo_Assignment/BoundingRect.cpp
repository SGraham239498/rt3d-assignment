#include "BoundingRect.h"


BoundingRect::BoundingRect(void)
{
}


BoundingRect::~BoundingRect(void)
{
}

rt3d::materialStruct rectMaterial = {
	{0.9f, 0.9f, 0.9f, 1.0f}, // ambient
	{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse
	{1.0f, 0.8f, 0.8f, 1.0f}, // specular
	2.0f  // shininess
};


void BoundingRect::init(GLuint& shader, GLuint& meshToUse, GLuint& indexToUse, float x, float z, glm::vec3& ps)
{
	pos = ps;
	mR = 90.0f;
	mesh = meshToUse;
	index = indexToUse;
	rt3d::setMaterial(shader, rectMaterial);
	scaleX = x;
	scaleZ = z;
	width = scaleX*1.1;
	depth = scaleZ*1.1;
}


float BoundingRect::xPos(){
	return pos.x;
}

float BoundingRect::yPos(){
	return pos.y;
}

float BoundingRect::zPos(){
	return pos.z;
}

void BoundingRect::update()
{
	
}

void BoundingRect::handleEvent(Character * player)
{
	// detect whether a character is inside the area of the rectangle
	if ( (player->xPos() >= this->xPos() - width) && (player->xPos() <= this->xPos() + width) 
		&& (player->zPos() <= this->zPos() + depth) && (player->zPos() >= this->zPos() - depth) )
		// set the characters position based on the rotation of the character to prevent it from entering the rectangle
		player->setPos(
		glm::vec3(player->xPos() - std::sin(player->getcR()*DEG_TO_RAD)/10, player->yPos(), player->zPos() + std::cos(player->getcR()*DEG_TO_RAD)/10));
	
}

void BoundingRect::draw(GLuint& shader, glm::mat4& mainMatrix)
{
	// draw bounding rectangle for debugging purposes
	
	mvStack.push(mainMatrix);
	glBindTexture(GL_TEXTURE_2D, 7);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(scaleX, 1.0f, scaleZ));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(mesh, index, GL_LINE_LOOP);
	mvStack.pop();
}
