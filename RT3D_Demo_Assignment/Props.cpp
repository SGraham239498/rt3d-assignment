#include "Props.h"

rt3d::materialStruct PropsMaterial = {
	{0.9f, 0.9f, 0.9f, 1.0f}, // ambient
	{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse
	{1.0f, 0.8f, 0.8f, 1.0f}, // specular
	2.0f  // shininess
};

Props::Props(void)
{
}


Props::~Props(void)
{
	
}

void Props::init(GLuint& shader)
{
	rt3d::setMaterial(shader, PropsMaterial);
	mR = 90;
	
}

glm::vec3 Props::getPos()
{
	return pos;
}

glm::vec3 Props::setPos(glm::vec3 newPos)
{
	pos = newPos;
	return pos;
}

float Props::xPos(){
	return pos.x;
}

float Props::yPos(){
	return pos.y;
}

float Props::zPos(){
	return pos.z;
}

void Props::loadBarrier(GLuint& shader)
{
	barrierIndex = 0;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	rt3d::loadObj("models/Props/barrier.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	barrierIndex = size;
	barrierMesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	barrierTex = rt3d::loadBitmapClamp("textures/barrier.bmp");
}
void Props::drawBarrier(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot)
{
	
	// draw barrier 
	glBindTexture(GL_TEXTURE_2D, barrierTex);
	mvStack.push(mainMatrix);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(1.0f, 1.2f, 1.0f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	for(int i = 0; i < 10; i++)
	{
		mvStack.push(mvStack.top());
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(2.7f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
		rt3d::drawMesh(barrierMesh, barrierIndex, GL_TRIANGLES);
	}
	for(int i = 0; i < 10; i++)
	{
		mvStack.pop();
	}
	mvStack.pop();
	
}

void Props::loadBridge(GLuint& shader)
{
	bridgeIndex = 0;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	rt3d::loadObj("models/Props/bridge.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	bridgeIndex = size;
	bridgeMesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	bridgeTex = rt3d::loadBitmapRepeat("textures/bridge.bmp");
}

void Props::drawBridge(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot)
{
	
	// draw bridge 
	glBindTexture(GL_TEXTURE_2D, bridgeTex);
	mvStack.push(mainMatrix);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.1f, 0.1f, 0.7f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(bridgeMesh, bridgeIndex, GL_TRIANGLES);
	mvStack.pop();

}

void Props::loadOldCar(GLuint& shader)
{
	oldCarIndex = 0;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	rt3d::loadObj("models/Props/oldCar.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	oldCarIndex = size;
	oldCarMesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	oldCarTex = rt3d::loadBitmapClamp("textures/oldCar.bmp");
}
void Props::drawOldCar(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot)
{
	
	// draw oldCar
	glBindTexture(GL_TEXTURE_2D, oldCarTex);
	mvStack.push(mainMatrix);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(1.1f, 1.1f, 1.1f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(oldCarMesh, oldCarIndex, GL_TRIANGLES);
	mvStack.pop();
	
}

void Props::loadFence(GLuint& shader)
{
	fenceIndex = 0;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	rt3d::loadObj("models/Props/fence.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	fenceIndex = size;
	fenceMesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	fenceTex = rt3d::loadBitmapClamp("textures/WoodFence.bmp");
}

void Props::drawFence(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot)
{
	
	// draw fence
	glBindTexture(GL_TEXTURE_2D, fenceTex);
	mvStack.push(mainMatrix);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.8f, 0.8f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	for(int i = 0; i < 10; i++)
	{
		mvStack.push(mvStack.top());
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 3.5f));
		rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
		rt3d::drawMesh(fenceMesh, fenceIndex, GL_TRIANGLES);
	}
	for(int i = 0; i < 10; i++)
	{
		mvStack.pop();
	}
	mvStack.pop();	
}

void Props::loadRock(GLuint& shader)
{
	rockIndex = 0;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	rt3d::loadObj("models/Props/rock2.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	rockIndex = size;
	rockMesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	rockTex = rt3d::loadBitmapRepeat("textures/rock5.bmp");
}

void Props::drawRock(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot)
{
	
	// draw Rock
	glBindTexture(GL_TEXTURE_2D, rockTex);
	mvStack.push(mainMatrix);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.6f, 0.6f, 0.6f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(rockMesh, rockIndex, GL_TRIANGLES);
	mvStack.pop();
	
}

void Props::loadBrokenWall(GLuint& shader)
{
	brokenWallIndex = 0;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	rt3d::loadObj("models/Props/brokenwall.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	brokenWallIndex = size;
	brokenWallMesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	brokenWallTex = rt3d::loadBitmapRepeat("textures/brokenwall.bmp");
}

void Props::drawBrokenWall(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot)
{
	
	// draw broken wall
	glBindTexture(GL_TEXTURE_2D, brokenWallTex);
	mvStack.push(mainMatrix);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.6f, 0.6f, 0.6f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	for(int i = 0; i < 5; i++)
	{
		mvStack.push(mvStack.top());
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 3.5f));
		rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
		rt3d::drawMesh(brokenWallMesh, brokenWallIndex, GL_TRIANGLES);
	}
	for(int i = 0; i < 5; i++)
	{
		mvStack.pop();
	}
	mvStack.pop();	

}
