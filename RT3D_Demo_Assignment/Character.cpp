#include "Character.h"

rt3d::materialStruct characterMaterial = {
	{0.9f, 0.9f, 0.9f, 1.0f}, // ambient
	{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse
	{1.0f, 0.8f, 0.8f, 1.0f}, // specular
	2.0f  // shininess
};

Character::Character()
{
	
}

void Character::init(GLuint& shader)
{
	walking = false;
	pos = glm::vec3(-26.0f,0.5f,-23.0f);
	speed = 0.1f;
	scale = 2.0f;
	mR = 90.0f;
	cR = 90.0f;
	md2VertCount = 0;
	currentAnim = 0;
	texture = rt3d::loadBitmapRepeat("hayden.bmp");
	mesh = tmpModel.ReadMD2Model("hayden-tris.MD2");
	md2VertCount = tmpModel.getVertDataSize();
	rt3d::setMaterial(shader, characterMaterial);
}


Character::~Character(void)
{
}

float Character::xPos(){
	return pos.x;
}

float Character::yPos(){
	return pos.y;
}

float Character::zPos(){
	return pos.z;
}

void Character::update()
{
	if (walking == true)
		currentAnim = 1;
	if(walking == false)
		currentAnim = 0;
	// set bounds for the scene
	if(this->pos.x <= -31)
		this->setPos(glm::vec3(pos.x+speed,pos.y,pos.z));
	 if (this->pos.x >= 21)
		 this->setPos(glm::vec3(pos.x-speed,pos.y,pos.z));
	 if(this->pos.z <= -24)
		this->setPos(glm::vec3(pos.x,pos.y,pos.z+speed));
	 if (this->pos.z >= 29)
		 this->setPos(glm::vec3(pos.x,pos.y,pos.z-speed));
}

void Character::handleEvent()
{
	
	Uint8 *keys = SDL_GetKeyboardState(NULL);	
	if ( keys[SDL_SCANCODE_UP] ){ pos = moveForward(pos, cR, speed); walking = true;} // move character forward
	if ( keys[SDL_SCANCODE_DOWN] ){ pos = moveForward(pos, cR, -speed); walking = true;} // move character backwards
	if ( keys[SDL_SCANCODE_A] ) { pos = strafe(pos, cR, -0.1f); walking = true;} // move character left
	if ( keys[SDL_SCANCODE_D] ) { pos = strafe(pos, cR, 0.1f); walking = true;} // move character right
	if ( keys[SDL_SCANCODE_LEFT] ){	cR -= 3.0f;} // rotate character left
	if ( keys[SDL_SCANCODE_RIGHT] ){ cR += 3.0f;} // rotate character right

	if ( !keys[SDL_SCANCODE_UP] && !keys[SDL_SCANCODE_DOWN] && !keys[SDL_SCANCODE_A] && !keys[SDL_SCANCODE_D] )walking = false;

	
}

// based on the camera movement from the rt3d labs

// move the character foward based on its rotation
glm::vec3 Character::moveForward(glm::vec3 pos, GLfloat angle, GLfloat d) {
	return glm::vec3(pos.x + d*std::sin(angle*DEG_TO_RAD), pos.y, pos.z - d*std::cos(angle*DEG_TO_RAD));
}

// move the character sideways based on its rotation
glm::vec3 Character::strafe(glm::vec3 pos, GLfloat angle, GLfloat d) {
	return glm::vec3(pos.x + d*std::cos(angle*DEG_TO_RAD), pos.y, pos.z + d*std::sin(angle*DEG_TO_RAD));
}

void Character::draw(GLuint& shader, glm::mat4& mainMatrix)
{
	// Animate the md2 model, and update the mesh with new vertex data
	tmpModel.Animate(currentAnim,0.1);
	rt3d::updateMesh(mesh,RT3D_VERTEX,tmpModel.getAnimVerts(),tmpModel.getVertDataSize());

	// draw hayden model
	glCullFace(GL_FRONT);
	glBindTexture(GL_TEXTURE_2D, texture);
	mvStack.push(mainMatrix);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), -cR, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(), mR, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(), -mR, glm::vec3(1.0f, 0.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(scale*0.01, scale*0.01, scale*0.01));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(mesh, md2VertCount, GL_TRIANGLES);
	mvStack.pop();
	glCullFace(GL_BACK);
}
