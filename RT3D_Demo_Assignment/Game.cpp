#include "Game.h"
	

using namespace std;


rt3d::lightStruct light0 = {
	{0.5f, 0.5f, 0.5f, 1.0f}, // ambient
	{0.9f, 0.7f, 0.7f, 1.0f}, // diffuse
	{1.0f, 0.8f, 0.8f, 1.0f}, // specular
	{-10.0f, 4.0f, 1.0f, 1.0f}  // position
};

rt3d::materialStruct material0 = {
	{0.9f, 0.9f, 0.9f, 1.0f}, // ambient
	{0.7f, 0.5f, 0.5f, 1.0f}, // diffuse
	{1.0f, 0.8f, 0.8f, 1.0f}, // specular
	2.0f  // shininess
};


Game::Game(void)
{
	
}

Game::~Game()
{	
	delete camera;
	delete player;
	delete buildings;
	delete props;
	SDL_Quit();
}

	 
// Set up rendering context
SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
    // Not able to use SDL to choose profile (yet), should default to core profile on 3.2 or later
	// If you request a context not supported by your drivers, no OpenGL context will be created
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering

    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

void Game::init(void)
 {
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	
	// initialize main shaders
	shaderProgram = rt3d::initShaders("phong-tex.vert","phong-tex.frag");
	skyboxShader = rt3d::initShaders("skybox.vert","skybox.frag");
	groundTex = rt3d::loadBitmapRepeat("dry_earth.bmp");
	
	r = -90.0f;
	rot90 = 90.0f;
	scale = 2.0f;
		
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);

	//load the ground plane model
	meshIndex = 0;
	vector<GLfloat> verts;
	vector<GLfloat> norms;
	vector<GLfloat> tex_coords;
	vector<GLuint> indices;
	rt3d::loadObj("groundPlane.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	meshIndex = size;	
	groundMesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
			
	skybox = new SkyBox();
	skybox->setup();
	player = new Character();
	player->init(shaderProgram);
	camera = new Camera(player);
	buildings = new Building();
	props = new Props();

	for (int i = 0; i < 10; i++)
		boundingRect[i] = new BoundingRect();
	//load buildings
	buildings->init(shaderProgram);
	buildings->loadShanty(shaderProgram);
	buildings->loadRuin(shaderProgram);
	buildings->loadBrokenRuin(shaderProgram);
	buildings->loadTowerBlock(shaderProgram);
	buildings->loadTowerBlock2(shaderProgram);
	buildings->loadDestroyedBuilding(shaderProgram);
	//load props
	props->init(shaderProgram);
	props->loadBarrier(shaderProgram);
	props->loadBridge(shaderProgram);
	props->loadOldCar(shaderProgram);
	props->loadFence(shaderProgram);
	props->loadRock(shaderProgram);
	props->loadBrokenWall(shaderProgram);
	// set up bounding rectangle scale and coordinates
	// Ideally these would belong to objects to encapsulate their properties within each object
	// and simplify their initialization, it would also make it easier to identify the object to which it belongs
	boundingRect[0]->init(shaderProgram, groundMesh, meshIndex, 1.0f, 1.0f, glm::vec3(-25.0f, 0.5f, 10.0f));//car
	boundingRect[1]->init(shaderProgram, groundMesh, meshIndex, 1.0f, 1.0f, glm::vec3(15.0f, 0.5f, 10.0f));//car
	boundingRect[2]->init(shaderProgram, groundMesh, meshIndex, 18.0f, 4.7f, glm::vec3(-22.0f,0.5f,-10.0f));//houses
	boundingRect[3]->init(shaderProgram, groundMesh, meshIndex, 3.0f, 3.0f, glm::vec3(20.0f, 0.5f, -11.0f));//rock
	boundingRect[4]->init(shaderProgram, groundMesh, meshIndex, 3.0f, 3.0f, glm::vec3(10.0f, 0.5f, 19.0f));//rock
	boundingRect[5]->init(shaderProgram, groundMesh, meshIndex, 3.0f, 3.0f, glm::vec3(-30.0f, 0.5f, 0.0f));//rock
	boundingRect[6]->init(shaderProgram, groundMesh, meshIndex, 8.0f, 0.2f, glm::vec3(-18.0f, 0.5f, 5.0f));//wall
	boundingRect[7]->init(shaderProgram, groundMesh, meshIndex, 0.2f, 8.0f, glm::vec3(-12.0f, 0.5f, 8.0f));//wall
	boundingRect[8]->init(shaderProgram, groundMesh, meshIndex, 0.7f, 2.0f, glm::vec3(-11.0f, 0.5f, 28.0f));//bridge
	boundingRect[9]->init(shaderProgram, groundMesh, meshIndex, 0.7f, 2.0f, glm::vec3(-7.0f, 0.5f, 28.0f));//bridge
	
 }

void Game::update()
{
	camera->update();
	player->update();
}

void Game::handleEvent()
{
	camera->handleEvent();
	player->handleEvent();
	for (int i = 0; i < 10; i++)
	boundingRect[i]->handleEvent(player);
}

void Game::draw(SDL_Window * window) {
	// clear the screen
	 
	glClearColor(0.5f, 0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//setup projection matrix
	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,200.0f);

	glm::mat4 modelview(1.0);
	mvStack.push(modelview);

	view = camera->setViewMatrix();	
	mvStack.top() = view;

	glUseProgram(skyboxShader);
	rt3d::setUniformMatrix4fv(skyboxShader,"projection", glm::value_ptr(projection));
		
// draw the skybox	

glDepthMask(GL_FALSE); // make sure depth test is off
glm::mat3 mvRotOnlyMat3 = glm::mat3(mvStack.top()); // extract rotation matrix from view matrix
mvStack.push( glm::mat4(mvRotOnlyMat3) );

// front
glBindTexture(GL_TEXTURE_2D, skybox->front());
mvStack.top() = glm::scale(mvStack.top(), glm::vec3(scale, scale, 1));
mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, -2.0f));
mvStack.top() = glm::rotate(mvStack.top(), r, glm::vec3(1.0f, 0.0f, 0.0f));
rt3d::setUniformMatrix4fv(skyboxShader, "modelview", glm::value_ptr(mvStack.top()));
rt3d::drawIndexedMesh(skybox->mesh(),meshIndex,GL_TRIANGLES);
mvStack.pop();

// back
mvStack.push( glm::mat4(mvRotOnlyMat3) );
glBindTexture(GL_TEXTURE_2D, skybox->back());
mvStack.top() = glm::scale(mvStack.top(), glm::vec3(scale, scale, 1));
mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 2.0f));
mvStack.top() = glm::rotate(mvStack.top(), r*2, glm::vec3(0.0f, 1.0f, 0.0f));
mvStack.top() = glm::rotate(mvStack.top(), r, glm::vec3(1.0f, 0.0f, 0.0f));
rt3d::setUniformMatrix4fv(skyboxShader, "modelview", glm::value_ptr(mvStack.top()));
rt3d::drawIndexedMesh(skybox->mesh(),meshIndex,GL_TRIANGLES);
mvStack.pop();

// left
mvStack.push( glm::mat4(mvRotOnlyMat3) );
glBindTexture(GL_TEXTURE_2D, skybox->left());
mvStack.top() = glm::scale(mvStack.top(), glm::vec3(1, scale, scale));
mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-2.0f, 0.0f, 0.0f));
mvStack.top() = glm::rotate(mvStack.top(), r*3, glm::vec3(0.0f, 1.0f, 0.0f));
mvStack.top() = glm::rotate(mvStack.top(), r, glm::vec3(1.0f, 0.0f, 0.0f));
rt3d::setUniformMatrix4fv(skyboxShader, "modelview", glm::value_ptr(mvStack.top()));
rt3d::drawIndexedMesh(skybox->mesh(),meshIndex,GL_TRIANGLES);
mvStack.pop();

// right
mvStack.push( glm::mat4(mvRotOnlyMat3) );
glBindTexture(GL_TEXTURE_2D, skybox->right());
mvStack.top() = glm::scale(mvStack.top(), glm::vec3(1, scale, scale));
mvStack.top() = glm::translate(mvStack.top(), glm::vec3(2.0f, 0.0f, 0.0f));
mvStack.top() = glm::rotate(mvStack.top(), r, glm::vec3(0.0f, 1.0f, 0.0f));
mvStack.top() = glm::rotate(mvStack.top(), r, glm::vec3(1.0f, 0.0f, 0.0f));
rt3d::setUniformMatrix4fv(skyboxShader, "modelview", glm::value_ptr(mvStack.top()));
rt3d::drawIndexedMesh(skybox->mesh(),meshIndex,GL_TRIANGLES);
mvStack.pop();

// top
mvStack.push( glm::mat4(mvRotOnlyMat3) );
glBindTexture(GL_TEXTURE_2D, skybox->top());
mvStack.top() = glm::scale(mvStack.top(), glm::vec3(scale, 1, scale));
mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 2.0f, 0.0f));
mvStack.top() = glm::rotate(mvStack.top(), r, glm::vec3(0.0f, 0.0f, 1.0f));
rt3d::setUniformMatrix4fv(skyboxShader, "modelview", glm::value_ptr(mvStack.top()));
rt3d::drawIndexedMesh(skybox->mesh(),meshIndex,GL_TRIANGLES);
mvStack.pop();


glDepthMask(GL_TRUE); // make sure depth test is off

	// switch to main shader
	glUseProgram(shaderProgram);
	// reset projection matrix, lighting and material 
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,200.0f);
	rt3d::setUniformMatrix4fv(shaderProgram,"projection", glm::value_ptr(projection));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);

	//draw the ground
	mvStack.push(mvStack.top()); 
	glBindTexture( GL_TEXTURE_2D, groundTex);
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(), glm::vec3(80.0f, 80.0f, 80.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(groundMesh,meshIndex,GL_TRIANGLES);
	mvStack.pop();

	//draw player
	player->draw(shaderProgram, mvStack.top());

	//draw shanty buildings
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-30.0f,0.0f,-30.0f), 0);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-23.0f,0.0f,-30.0f), 0);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-16.0f,0.0f,-30.0f), 0);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-9.0f,0.0f,-30.0f), 0);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-30.0f,0.0f,-10.0f), rot90*2);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-23.0f,0.0f,-10.0f), rot90*2);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-16.0f,0.0f,-10.0f), rot90*2);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-9.0f,0.0f,-10.0f), rot90*2);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-45.0f,0.0f, 18.0f), rot90);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-45.0f,0.0f, 11.0f), rot90);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-45.0f,0.0f, 4.0f), rot90);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-45.0f,0.0f,-3.0f), rot90);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-45.0f,0.0f,-10.0f), rot90);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-45.0f,0.0f,-17.0f), rot90);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-45.0f,0.0f,-24.0f), rot90);
	buildings->drawShanty(shaderProgram, mvStack.top(), glm::vec3(-45.0f,0.0f,-31.0f), rot90);

	//draw ruin buildings
	buildings->drawRuin(shaderProgram, mvStack.top(), glm::vec3(30.0f,-0.2f,-25.0f), rot90*2.5);
	buildings->drawBrokenRuin(shaderProgram, mvStack.top(), glm::vec3(25.0f,0.0f,10.0f), rot90);
	buildings->drawBrokenRuin(shaderProgram, mvStack.top(), glm::vec3(25.0f,0.0f,0.0f), 0);
	buildings->drawBrokenRuin(shaderProgram, mvStack.top(), glm::vec3(30.0f,0.0f,10.0f), rot90*2);
	buildings->drawBrokenRuin(shaderProgram, mvStack.top(), glm::vec3(30.0f,0.0f,0.0f), -rot90);

	//draw tower block buildings
	buildings->drawTowerBlock(shaderProgram, mvStack.top(), glm::vec3(-25.0f,-0.2f,40.0f), rot90*2);
	buildings->drawTowerBlock(shaderProgram, mvStack.top(), glm::vec3(-85.0f,-0.2f,5.0f), rot90);
	buildings->drawTowerBlock(shaderProgram, mvStack.top(), glm::vec3(-105.0f,-0.2f,-25.0f), rot90);
	buildings->drawTowerBlock2(shaderProgram, mvStack.top(), glm::vec3(15.0f,-0.2f,45.0f), rot90*2);
	buildings->drawTowerBlock2(shaderProgram, mvStack.top(), glm::vec3(120.0f,-0.2f,55.0f), rot90*3);
	buildings->drawTowerBlock2(shaderProgram, mvStack.top(), glm::vec3(120.0f,-0.2f,-5.0f), rot90*2);
	buildings->drawTowerBlock2(shaderProgram, mvStack.top(), glm::vec3(150.0f,-0.2f,-65.0f), rot90*2);	

	//draw destroyed buildings
	buildings->drawDestroyedBuilding(shaderProgram, mvStack.top(), glm::vec3(-75.0f,3.0f,-60.0f), -rot90*3.6);
	buildings->drawDestroyedBuilding(shaderProgram, mvStack.top(), glm::vec3(-40.0f,0.0f,-70.0f), -rot90);
	buildings->drawDestroyedBuilding(shaderProgram, mvStack.top(), glm::vec3(0.0f,0.0f,-70.0f), rot90*1.6);
	buildings->drawDestroyedBuilding(shaderProgram, mvStack.top(), glm::vec3(40.0f,0.0f,-70.0f), -rot90);

	//draw barriers
	props->drawBarrier(shaderProgram, mvStack.top(), glm::vec3(-7.0f,0.0f,-25.0f), 0);
	props->drawBarrier(shaderProgram, mvStack.top(), glm::vec3(22.0f,0.0f,-25.0f), -rot90);
	props->drawBarrier(shaderProgram, mvStack.top(), glm::vec3(22.0f,0.0f, 2.0f), -rot90);
	props->drawBarrier(shaderProgram, mvStack.top(), glm::vec3(-7.0f,0.0f, 30.0f), 0);
	props->drawBarrier(shaderProgram, mvStack.top(), glm::vec3(-40.0f,0.0f, 30.0f), 0);
	props->drawBarrier(shaderProgram, mvStack.top(), glm::vec3(-32.0f,0.0f, 1.0f), -rot90);

	//draw bridge
	props->drawBridge(shaderProgram, mvStack.top(), glm::vec3(-9.0f, 0.0f, 35.0f), 0);

	//draw old car
	props->drawOldCar(shaderProgram, mvStack.top(), glm::vec3(-33.0f, 0.0f, -18.0f), rot90*2.2);
	props->drawOldCar(shaderProgram, mvStack.top(), glm::vec3(-33.0f, 0.0f, -22.0f), rot90*1.4);
	props->drawOldCar(shaderProgram, mvStack.top(), glm::vec3(-25.0f, 0.0f, 10.0f), rot90*1.4);
	props->drawOldCar(shaderProgram, mvStack.top(), glm::vec3(15.0f, 0.0f, 10.0f), -rot90*1.8);

	//draw fences
	props->drawFence(shaderProgram, mvStack.top(), glm::vec3(-35.0f, 0.0f, -5.0f), rot90);

	//draw rocks
	props->drawRock(shaderProgram, mvStack.top(), glm::vec3(-30.0f, 0.0f, 0.0f), 0);
	props->drawRock(shaderProgram, mvStack.top(), glm::vec3(20.0f, 0.0f, -10.0f), 0);
	props->drawRock(shaderProgram, mvStack.top(), glm::vec3(10.0f, 0.0f, 19.0f), 0);

	//draw walls
	props->drawBrokenWall(shaderProgram, mvStack.top(), glm::vec3(-22.0f, 0.0f, 5.0f), rot90);
	props->drawBrokenWall(shaderProgram, mvStack.top(), glm::vec3(-12.0f, 0.0f, 5.0f), 0);

	//draw bounding rectangles for debugging
	//for (int i = 0; i < 10; i++)
	//boundingRect[i]->draw(shaderProgram, mvStack.top());

	mvStack.pop(); //initial matrix
	SDL_GL_SwapWindow(window); // swap buffers
}

 void Game::run(void) 
{
	SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
    hWindow = setupRC(glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;

	this->init();

	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
		}
		this->update();
		this->handleEvent();
		this->draw(hWindow); // call the draw function
	}
	
}

   
