#pragma once
#include "rt3d.h"
#include "rt3dObjLoader.h"
#include "md2model.h"

class Character
{
public:
	Character();
	~Character(void);
	void init(GLuint& shader);
	glm::vec3 getPos();
	void setPos(glm::vec3 newPos) {pos = newPos;}
	float xPos();
	float yPos();
	float zPos();
	float getcR(){return cR;}
	void update();
	void handleEvent();
	void draw(GLuint& shader, glm::mat4& mainMatrix);
	glm::vec3 moveForward(glm::vec3 pos, GLfloat angle, GLfloat d);
	glm::vec3 strafe(glm::vec3 pos, GLfloat angle, GLfloat d);
	float getSpeed(){return speed;}
	void setSpeed(float newSpeed){ speed = newSpeed;}
private:
	glm::vec3 pos;
	std::stack<glm::mat4> mvStack;
	md2model tmpModel;
	float scale;
	float mR; // model rotation
	float cR; // character rotation
	int currentAnim;
	GLuint md2VertCount;
	GLuint mesh;
	GLuint texture;
	bool walking;
	float speed;
};

