#include "Building.h"

rt3d::materialStruct buildingMaterial = {
	{0.9f, 0.9f, 0.9f, 1.0f}, // ambient
	{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse
	{1.0f, 0.8f, 0.8f, 1.0f}, // specular
	2.0f  // shininess
};

Building::Building(void)
{
}


Building::~Building(void)
{
	
}

void Building::init(GLuint& shader)
{
	rt3d::setMaterial(shader, buildingMaterial);
	mR = 90;
	
}

glm::vec3 Building::getPos()
{
	return pos;
}

glm::vec3 Building::setPos(glm::vec3 newPos)
{
	pos = newPos;
	return pos;
}

float Building::xPos(){
	return pos.x;
}

float Building::yPos(){
	return pos.y;
}

float Building::zPos(){
	return pos.z;
}

void Building::loadShanty(GLuint& shader)
{
	shantyIndex = 0;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	rt3d::loadObj("models/Buildings/shantyhouse.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	shantyIndex = size;
	shantyMesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	shantyTex = rt3d::loadBitmapClamp("textures/shanty_building.bmp");
}
void Building::drawShanty(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot)
{
	
	// draw shanty building
	glBindTexture(GL_TEXTURE_2D, shantyTex);
	mvStack.push(mainMatrix);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.01f, 0.007f, 0.01f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(shantyMesh, shantyIndex, GL_TRIANGLES);
	mvStack.pop();
	
}

void Building::loadRuin(GLuint& shader)
{
	ruinIndex = 0;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	rt3d::loadObj("models/Buildings/ruins.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	ruinIndex = size;
	ruinMesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	ruinTex = rt3d::loadBitmapClamp("textures/ruin.bmp");
}
void Building::drawRuin(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot)
{
	
	// draw ruin building
	glBindTexture(GL_TEXTURE_2D, ruinTex);
	mvStack.push(mainMatrix);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(ruinMesh, ruinIndex, GL_TRIANGLES);
	mvStack.pop();
	
}

void Building::loadBrokenRuin(GLuint& shader)
{
	brokenRuinIndex = 0;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	rt3d::loadObj("models/Buildings/brokenruins.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	brokenRuinIndex = size;
	brokenRuinMesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	
}

void Building::drawBrokenRuin(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot)
{
	
	// draw broken ruin building
	glBindTexture(GL_TEXTURE_2D, ruinTex);
	mvStack.push(mainMatrix);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(brokenRuinMesh, brokenRuinIndex, GL_TRIANGLES);
	mvStack.pop();
	
}

void Building::loadTowerBlock(GLuint& shader)
{
	towerBlockIndex = 0;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	rt3d::loadObj("models/Buildings/towerblock.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	towerBlockIndex = size;
	towerBlockMesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	towerBlockTex = rt3d::loadBitmapRepeat("textures/towerblock.bmp");
}

void Building::drawTowerBlock(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot)
{
	
	// draw tower block building
	glBindTexture(GL_TEXTURE_2D, towerBlockTex);
	mvStack.push(mainMatrix);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.1f, 0.1f, 0.1f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(towerBlockMesh, towerBlockIndex, GL_TRIANGLES);
	mvStack.pop();
	
}

void Building::loadTowerBlock2(GLuint& shader)
{
	towerBlock2Index = 0;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	rt3d::loadObj("models/Buildings/towerBlock2.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	towerBlock2Index = size;
	towerBlock2Mesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	towerBlock2Tex = rt3d::loadBitmapRepeat("textures/towerBlock2.bmp");
}

void Building::drawTowerBlock2(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot)
{
	
	// draw tower block 2 building
	glBindTexture(GL_TEXTURE_2D, towerBlock2Tex);
	mvStack.push(mainMatrix);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.1f, 0.1f, 0.1f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(towerBlock2Mesh, towerBlock2Index, GL_TRIANGLES);
	mvStack.pop();
	
}

void Building::loadDestroyedBuilding(GLuint& shader)
{
	destroyedBuildingIndex = 0;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	rt3d::loadObj("models/Buildings/destroyedbuilding.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	destroyedBuildingIndex = size;
	destroyedBuildingMesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	destroyedBuildingTex = rt3d::loadBitmapClamp("textures/flats.bmp");
}

void Building::drawDestroyedBuilding(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot)
{
	
	// draw destroyed building
	glBindTexture(GL_TEXTURE_2D, destroyedBuildingTex);
	mvStack.push(mainMatrix);
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), rot, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(2.5f, 3.5f, 3.0f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(destroyedBuildingMesh, destroyedBuildingIndex, GL_TRIANGLES);
	mvStack.pop();
	
}