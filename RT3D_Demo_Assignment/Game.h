#pragma once
// Real Time 3D Assignemnt 2013
// Created by B00239498

// C stdlib and C time libraries for rand and time functions
#include <cstdlib>
#include <ctime>
#include "rt3d.h"
#include "rt3dObjLoader.h"
#include "Camera.h"
#include "SkyBox.h"
#include "Character.h"
#include "Building.h"
#include "Props.h"
#include "BoundingRect.h"

class Game
{
public:	

	Game(void);
	~Game();
	void init(void);
	void run(void); 
	void handleEvent();	
	void update();
	void draw(SDL_Window * window);
	GLuint shader(){return shaderProgram;}				
		
private:

	SDL_Window * window;
	SDL_GLContext sdlcontext;	
	GLuint skyboxShader;
	GLuint shaderProgram;
	Camera * camera;
	glm::mat4 view;
	SkyBox * skybox;
	std::stack<glm::mat4> mvStack;
	GLuint groundMesh;
	GLuint meshIndex;
	GLfloat r;
	GLuint groundTex;
	float scale;
	Character * player;
	Building * buildings;
	Props * props;
	BoundingRect * boundingRect[10];
	float rot90;
};