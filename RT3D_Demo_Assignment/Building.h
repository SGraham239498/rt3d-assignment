#pragma once
#include "rt3d.h"
#include "rt3dObjLoader.h"

// Ideally each building would be a seperate object and possibly inherit from a generic class
class Building 
{
public:
	Building(void);
	virtual ~Building(void);
	void init(GLuint& shader);
	glm::vec3 getPos();
	glm::vec3 setPos(glm::vec3 newPos);
	float xPos();
	float yPos();
	float zPos();
	void loadShanty(GLuint& shader);
	void drawShanty(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot);
	void loadRuin(GLuint& shader);
	void drawRuin(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot);
	void loadBrokenRuin(GLuint& shader);
	void drawBrokenRuin(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot);
	void loadTowerBlock(GLuint& shader);
	void drawTowerBlock(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot);
	void loadTowerBlock2(GLuint& shader);
	void drawTowerBlock2(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot);
	void loadDestroyedBuilding(GLuint& shader);
	void drawDestroyedBuilding(GLuint& shader, glm::mat4& mainMatrix, glm::vec3& pos, float rot);
private:
	glm::vec3 pos;
	std::stack<glm::mat4> mvStack;
	float scale;
	float mR; // model rotation
	GLuint shantyIndex, towerIndex, ruinIndex, brokenRuinIndex, towerBlockIndex, towerBlock2Index, destroyedBuildingIndex;
	GLuint shantyMesh, towerMesh, ruinMesh, brokenRuinMesh, towerBlockMesh, towerBlock2Mesh, destroyedBuildingMesh;
	GLuint shantyTex, towerTex, ruinTex, towerBlockTex, towerBlock2Tex, destroyedBuildingTex;
};

