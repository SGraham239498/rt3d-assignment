#include "Game.h"

// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

// Program entry point 
int main(int argc, char *argv[]) {
 
	Game *newgame = new Game();
	    

	//newgame->init(); // initialise OpenGL and game variables
	
	newgame->run();
	
	
	delete newgame;
    
	return 0;

}

